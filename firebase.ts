// Import the functions you need from the SDKs you need
import { initializeApp, getApp, getApps } from 'firebase/app'
import { getFirestore } from 'firebase/firestore'
import { getAuth } from 'firebase/auth'

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBwLXE0ih4SaFdCuMba-pvMv0a6NpJJPOE",
  authDomain: "netflix-clone-42222.firebaseapp.com",
  projectId: "netflix-clone-42222",
  storageBucket: "netflix-clone-42222.appspot.com",
  messagingSenderId: "322689778631",
  appId: "1:322689778631:web:cc7dfedccd49f1ddba4fe2"
};

// Initialize Firebase
const app = !getApps().length ? initializeApp(firebaseConfig) : getApp()
const db = getFirestore()
const auth = getAuth()

export default app
export { auth, db }